using namespace std;
#ifndef CONFIG_HPP
#define CONFIG_HPP

#define TMP_FOLDER "/tmp/ep2"
#define ACTIVE_USERS_FOLDER "/tmp/ep2/active-users"
#define LOG_PATH "/tmp/ep2/logs"
#define TIMEOUT_IN_SECONDS 120
#define HEARTBEAT_FREQUENCY_IN_SECONDS 60

#endif