Thiago Guerrero
NUSP: 11275297

Daniel Leal
NUSP: 11321180

Para gerar os executáveis na raiz do projeto, rode:
$ make

Para executar o servidor, rode com permissão de root:
$ ./server <Porta TCP> <Porta UDP> &

Para executar o cliente, rode com permissão de root:
$ ./client <IP Servidor> <Porta Servidor> <TCP|UDP> <Porta P2P>

Para encerrar a execução do servidor, encerre o processo com SIGTERM

Para encerrar a execução do servidor de forma incorreta, encerre o processo com SIGKILL
